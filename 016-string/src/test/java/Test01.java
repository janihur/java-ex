import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Test01 {
    @Test
    @DisplayName("displayName - 1st implementation")
    public void displayName1() {
        assertThat("firstname, lastname", Main.displayName1("john.smith@example.com"), is("smith john"));
        assertThat("firstname, middlename, lastname", Main.displayName1("john.x.smith@example.com"), is("smith john x"));
        assertThat("firstname, middlename, lastname, ext", Main.displayName1("john.x.smith.ext@example.com"), is("smith john x ext"));
        assertThat("firstname, lastname, ext", Main.displayName1("john.smith.ext@example.com"), is("smith john ext"));
    }

    @Test
    @DisplayName("displayName - 2nd implementation")
    public void displayName2() {
        assertEquals("smith john", Main.displayName2("john.smith@example.com"));
        assertEquals("smith john x", Main.displayName2("john.x.smith@example.com"));
        assertEquals("smith john x ext", Main.displayName2("john.x.smith.ext@example.com"));
        assertEquals("smith john ext", Main.displayName2("john.smith.ext@example.com"));
    }

    @Test
    @DisplayName("java.lang.String.contains()")
    public void contains1() {
        String x = "Lorem ipsum. Dolor sit amet.";

        assertThat("no match", x.contains("dolor sit amet"), is(false));
        assertThat("match", x.toLowerCase().contains("dolor sit amet"), is(true));
    }
}
