import java.util.ArrayList;
import java.util.List;

public class Main {
    public static String displayName1(String email) {
        boolean isExt = email.contains(".ext@");
        String[] parts = email.split("(\\.ext)?@");
        String name = parts[0];

        String[] nameParts = name.split("\\.");

        String displayName = "";
        switch (nameParts.length) {
            case 2: {
                displayName = String.join(" ", new String[] {nameParts[1], nameParts[0]});
                break;
            }
            case 3: {
                displayName = String.join(" ", new String[] {nameParts[2], nameParts[0], nameParts[1]});
                break;
            }
        }

        return isExt ? displayName + " ext" : displayName;
    }

    public static String displayName2(String email) {
        String[] parts = email.split("(\\.ext)?@");
        String name = parts[0];

        String[] nameParts = name.split("\\.");

        List<String> displayName = new ArrayList<>();
        switch (nameParts.length) {
            case 2:
                displayName.add(nameParts[1]);
                displayName.add(nameParts[0]);
                break;
            case 3:
                displayName.add(nameParts[2]);
                displayName.add(nameParts[0]);
                displayName.add(nameParts[1]);
                break;
        }

        if (email.contains(".ext@")) {
            displayName.add("ext");
        }

        return String.join(" ", displayName);
    }
}
