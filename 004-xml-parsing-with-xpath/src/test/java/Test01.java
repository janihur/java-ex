import java.io.InputStream;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import org.junit.Test;

public class Test01 {
    private InputStream readResourceFile(String filename) {
	ClassLoader classLoader = getClass().getClassLoader();
	InputStream in = classLoader.getResourceAsStream(filename);
	return in;
    }

    private String toStrParam(Node n, String prefix) {
	NamedNodeMap attrs = n.getAttributes();
	return
	    prefix +
	    attrs.getNamedItem("name").getNodeValue() +
	    " " +
	    attrs.getNamedItem("value").getNodeValue()
	    ;
    }

    private Set<String> toStrParams(NodeList nodes, String prefix) {
	Set<String> strParams = new HashSet<String>();
	for (int i = 0; i < nodes.getLength(); i++) {
	    Node n = nodes.item(i);
	    strParams.add(toStrParam(n, prefix));
	}
	return strParams;
    }
    
    @Test
    public void test01() {
	InputStream xmlStream = readResourceFile("01.xml");
	assertNotNull(xmlStream);

	Set<String> params = new HashSet<String>();

	try {
	    DocumentBuilderFactory factory =
		DocumentBuilderFactory.newInstance();
	    factory.setNamespaceAware(true);
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    assertNotNull(builder);
	
	    Document doc = builder.parse(xmlStream);
	    XPath xpath = XPathFactory.newInstance().newXPath();
	    assertNotNull(xpath);
	    
	    xpath.setNamespaceContext(new NamespaceContext() {
		    public String getNamespaceURI(String prefix) {
			return prefix.equals("ilink") ? "http://www.comptel.com/ilink/api/soap/2005/09" : null;
		    }

		    public Iterator<?> getPrefixes(String val) {
			return null;
		    }

		    public String getPrefix(String uri) {
			return null;
		    }
		});

	    { // requested parameters (3 pcs)
		NodeList xmlParams = (NodeList)xpath.evaluate("/ilink:RESPONSE/ilink:RequestedParameters/ilink:Parameter", doc, XPathConstants.NODESET);
		Set<String> strParams = toStrParams(xmlParams, "@REQ@");

		assertThat("Number of xmlParams", xmlParams.getLength(), is(3));
		assertThat("Number strParams", strParams.size(), is (3));

		params.addAll(strParams);
	    }
	    { // new parameters (26 pcs)
		NodeList xmlParams = (NodeList)xpath.evaluate("/ilink:RESPONSE/ilink:NewParameters/ilink:Parameter", doc, XPathConstants.NODESET);
		Set<String> strParams = toStrParams(xmlParams, "@NEW@");

		assertThat("Number of xmlParams", xmlParams.getLength(), is(26));
		assertThat("Number strParams", strParams.size(), is (26));

		params.addAll(strParams);
	    }
	} catch (Exception ex) {
	    fail();
	}

	assertThat("Number of params", params.size(), is(3 + 26));

	// System.out.println(String.join(":", params));
    }
}
