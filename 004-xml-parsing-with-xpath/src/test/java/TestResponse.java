import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import org.junit.Test;

public class TestResponse {
    private String inputStream2String(InputStream in) {
	try (BufferedReader br = new BufferedReader(new InputStreamReader(in))) {
	    StringBuilder out = new StringBuilder();
	    String line;
	    while ((line = br.readLine()) != null) {
		out.append(line).append("\n");
	    }
	    return out.toString();
	} catch (java.io.IOException ex) {
	    return null;
	}
    }

    private String readResourceFile(String filename) {
	ClassLoader classLoader = getClass().getClassLoader();
	InputStream in = classLoader.getResourceAsStream(filename);
	return inputStream2String(in);
    }

    @Test
    public void testResponseFile01() {
	String xmlString = readResourceFile("01.xml");

	String attributes = "";

	try {
	    Response resp = new Response(xmlString);
	    assertNotNull(resp);

	    Set<String> responseElemAttrs = resp.getResponseElemAttrs();
	    assertThat(responseElemAttrs.size(), is(12));

	    Set<String> responseParams = resp.getResponseParameters();
	    assertThat(responseParams.size(), is(5));

	    Set<String> requestedParams = resp.getRequestedParameters();
	    assertThat(requestedParams.size(), is(3));

	    Set<String> newParams = resp.getNewParameters();
	    assertThat(newParams.size(), is(26));

	    Set<String> allParams = new HashSet<String>();
	    allParams.addAll(responseElemAttrs);
	    allParams.addAll(responseParams);
	    allParams.addAll(requestedParams);
	    allParams.addAll(newParams);
	    assertThat(allParams.size(), is(12 + 5 + 3 + 26));

	    attributes = String.join(":", allParams);
	} catch (Exception ex) {
	    ex.printStackTrace();
	    fail();
	}

	System.out.println(attributes);
    }
}
