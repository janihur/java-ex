import java.io.StringReader;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class Response {
    public Response(String xmlString)
	throws java.io.IOException,
	       javax.xml.parsers.ParserConfigurationException,
	       org.xml.sax.SAXException
    {
	xpath = XPathFactory.newInstance().newXPath();

	xpath.setNamespaceContext(new NamespaceContext() {
		public String getNamespaceURI(String prefix) {
		    return
			prefix.equals("ilink") ?
			"http://www.comptel.com/ilink/api/soap/2005/09" :
			null
			;
		}

		public Iterator<?> getPrefixes(String val) {
		    return null;
		}

		public String getPrefix(String uri) {
		    return null;
		}
	    });

	factory = DocumentBuilderFactory.newInstance();
	factory.setNamespaceAware(true);

	builder = factory.newDocumentBuilder();

	InputSource xml = new InputSource(new StringReader(xmlString));
	doc = builder.parse(xml);
    }

    public Set<String> getResponseElemAttrs()
	throws javax.xml.xpath.XPathExpressionException
    {
	Node response = (Node)xpath.evaluate("/ilink:RESPONSE", doc, XPathConstants.NODE);
	NamedNodeMap attrs = response.getAttributes();

	Set<String> strParams = new HashSet<String>();

	for (int i = 0; i < attrs.getLength(); i++) {
	    Attr a = (Attr)attrs.item(i);
	    if (a.getName().startsWith("xmlns")) {
		continue;
	    }
	    strParams.add(a.getName() + " " + a.getValue());
	}

	return strParams;
    }

    public Set<String> getResponseParameters()
	throws javax.xml.xpath.XPathExpressionException
    {
	NodeList xmlParams = (NodeList)xpath.evaluate("/ilink:RESPONSE/ilink:ResponseParameters/ilink:Parameter", doc, XPathConstants.NODESET);
	return toStrParams(xmlParams, "");
    }

    public Set<String> getRequestedParameters()
	throws javax.xml.xpath.XPathExpressionException
    {
	NodeList xmlParams = (NodeList)xpath.evaluate("/ilink:RESPONSE/ilink:RequestedParameters/ilink:Parameter", doc, XPathConstants.NODESET);
	return toStrParams(xmlParams, "@REQ@");
    }

    public Set<String> getNewParameters()
	throws javax.xml.xpath.XPathExpressionException
    {
	NodeList xmlParams = (NodeList)xpath.evaluate("/ilink:RESPONSE/ilink:NewParameters/ilink:Parameter", doc, XPathConstants.NODESET);
	return toStrParams(xmlParams, "@NEW@");
    }

    private Set<String> toStrParams(NodeList nodes, String prefix) {
	Set<String> strParams = new HashSet<String>();
	for (int i = 0; i < nodes.getLength(); i++) {
	    Node n = nodes.item(i);
	    NamedNodeMap attrs = n.getAttributes();
	    strParams.add(prefix +
			  attrs.getNamedItem("name").getNodeValue() +
			  " " +
			  attrs.getNamedItem("value").getNodeValue());
	}
	return strParams;
    }

    private final XPath xpath;
    private final DocumentBuilderFactory factory;
    private final DocumentBuilder builder;
    private final Document doc;
}
