import java.util.Random;

public class PayloadGenerator {
    public static String[] run(int payloadSize) {
        Random random = new Random();

        String[] sentences = {
            "the cow jumped over the moon",
            "an apple a day keeps the doctor away",
            "four score and seven years ago",
            "snow white and the seven dwarfs",
            "i am at two with nature"
        };

        String[] payload = new String[payloadSize];

        for(int i = 0; i < payloadSize; i++) {
            payload[i] = sentences[random.nextInt(sentences.length)];
        }

        return payload;
    }
}