import java.util.Properties;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

public class Producer {
    private Properties properties = null;
    private String topic = null;
    private KafkaProducer<String, String> producer = null;

    public Producer(String brokers, String topic) {
        properties = new Properties();
        properties.setProperty("bootstrap.servers", brokers);
        properties.setProperty("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.setProperty("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        this.topic = topic;

        producer = new KafkaProducer<>(properties); 
    }

    public void send1(String payload) {
        try {
            ProducerRecord<String, String> record = new ProducerRecord<>(topic, payload);
            producer.send(record, new ProducerCallback());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void send2(String payload) {
        try {
            ProducerRecord<String, String> record = new ProducerRecord<>(topic, payload);
            RecordMetadata m = producer.send(record).get();
            System.err.println("Send record successfully:");
            System.err.println(str(m));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void send(String[] payloads) {
        try {
            for (String payload : payloads) {
                ProducerRecord<String, String> record = new ProducerRecord<>(topic, payload);
                producer.send(record, new ProducerCallback());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void close() {
        producer.close();
    }

    private class ProducerCallback implements Callback {
        @Override
        public void onCompletion(RecordMetadata recordMetadata, Exception e) {
            if (e != null) {
                System.err.println("Failed to send record:");
                e.printStackTrace();
            } else {
                System.err.println("Send record successfully:");
                System.err.println(str(recordMetadata));
            }
        }
    }

    private String str(RecordMetadata recordMetadata) {
        String s = "(";
        s += "(topic = " + recordMetadata.topic() + ")";
        s += "(partition = " + recordMetadata.partition() + ")";
        if (recordMetadata.hasTimestamp()) {
            s += "(timestamp = " + recordMetadata.timestamp() + ")";
        }
        if (recordMetadata.hasOffset()) {
            s += "(offset = " + recordMetadata.offset() + ")";
        }
        s += ")";
        return s;
    }

}