public class Main {
    public static void main(final String[] args) {
        Producer p = new Producer(args[0], args[1]);

        if (args.length == 3) {
            p.send2(args[2]);
        } else {
            p.send(PayloadGenerator.run(5));
        }

        p.close();
    }
}
