public class Main {
    public static boolean isInteger(String possibleInteger) {
        try {
            Integer.parseInt(possibleInteger);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
