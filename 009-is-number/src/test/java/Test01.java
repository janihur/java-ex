import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import org.junit.Test;

public class Test01 {
    @Test
    public void IsInteger() {
        {
            String s = null;
            assertThat("null", Main.isInteger(s), is(false));
        }
        {
            String s = "";
            assertThat("empty", Main.isInteger(s), is(false));
        }
        {
            String s = "foobar";
            assertThat(s, Main.isInteger(s), is(false));
        }
        {
            String s = "1.1";
            assertThat(s, Main.isInteger(s), is(false));
        }
        {
            String s = "1";
            assertThat(s, Main.isInteger(s), is(true));
        }
        {
            String s = "01";
            assertThat(s, Main.isInteger(s), is(true));
        }
    }
    @Test
    public void IntegerStringToInteger() {
        {
            String s = "1";
            Integer i = Integer.valueOf(s);
            assertThat(s, i, is(1));
        }
        {
            String s = "01";
            Integer i = Integer.valueOf(s);
            assertThat(s, i, is(1));
        }
        {
            String s = "001";
            Integer i = Integer.valueOf(s);
            assertThat(s, i, is(1));
        }
        {
            String s = "0010";
            Integer i = Integer.valueOf(s);
            assertThat(s, i, is(10));
        }
    }
}
