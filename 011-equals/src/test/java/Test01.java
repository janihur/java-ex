import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import org.junit.Test;

import java.util.ArrayList;

public class Test01 {
    @Test
    public void equalsTest() {
	    Person a1 = new Person();
	    a1.type = "HERO";
	    a1.name = new Name();
	    a1.name.firstName = "Batman";
	    a1.aliases = new ArrayList<Name>(1);
	    a1.aliases.add(new Name("Bruce", "Wayne"));
	    a1.attributes = new ArrayList<Attribute>(2);
	    a1.attributes.add(new Attribute("Strength","12"));
        a1.attributes.add(new Attribute("Dexterity","12"));

        System.err.println("a1: " + a1);

        // --------------------------------------------------------------------

        Person a2 = new Person();
        a2.type = "HERO";
        a2.name = new Name();
        a2.name.firstName = "Batman";
        a2.aliases = new ArrayList<Name>(1);
        a2.aliases.add(new Name("Bruce", "Wayne"));
        a2.attributes = new ArrayList<Attribute>(2);
        a2.attributes.add(new Attribute("Strength","12"));
        a2.attributes.add(new Attribute("Dexterity","12"));

        System.err.println("a2: " + a2);

        // --------------------------------------------------------------------

        Person b = new Person();
        b.type = "VILLAIN";
        b.name = new Name();
        b.name.firstName = "The Riddler";
        b.aliases = new ArrayList<Name>(1);
        b.aliases.add(new Name("Edward","Nygma"));
		b.attributes = new ArrayList<Attribute>(2);
		b.attributes.add(new Attribute("Strength","3"));
		b.attributes.add(new Attribute("Dexterity","4"));

        System.err.println("b: " + b);

        // --------------------------------------------------------------------

		assertThat(a1, isA(Person.class));
        assertThat(a2, isA(Person.class));
        assertThat(b, isA(Person.class));

        // --------------------------------------------------------------------

        assertThat(a1.name, isA(Name.class));
        assertThat(a2.name, isA(Name.class));
        assertThat(b.name, isA(Name.class));

        // --------------------------------------------------------------------

        assertEquals(a1.name, new Name("Batman", null));
        assertThat(a1.name.equals(new Name("Batman", null)), is(true));
        assertThat(a1.name.equals(b.name), is(false));

        // --------------------------------------------------------------------

        assertEquals(a1, a2);
        assertThat(a1.equals(a2), is(true));
        assertThat(a1.equals(b), is(false));

        // --------------------------------------------------------------------

        a1.attributes.get(0).value = "13";
        System.err.println("a1: " + a1);
        assertNotEquals(a1, a2);
    }
}
