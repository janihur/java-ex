import java.util.List;
import java.util.Objects;

public class Person {
    public String type = null;
    public Name name = null;
    public List<Name> aliases = null;
    public List<Attribute> attributes = null;

    public Person() {}

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Person)) return false;

        Person person = (Person) obj;

        return
            Objects.equals(type, person.type) &&
            Objects.equals(name, person.name) &&
            Objects.equals(aliases, person.aliases) &&
            Objects.equals(attributes, person.attributes)
        ;
    }

    @Override
    public String toString() {
        return "Person{" +
            "type='" + type + '\'' +
            ", name=" + name +
            ", aliases=" + aliases +
            ", attributes=" + attributes +
            '}';
    }
}
