import java.util.Objects;

public class Attribute {
    public String name = null;
    public String value = null;

    public Attribute() {}

    public Attribute(String name, String value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Attribute)) return false;

        Attribute attribute = (Attribute) obj;

        return
            Objects.equals(this.name, attribute.name) &&
            Objects.equals(this.value, attribute.value)
        ;
    }

    @Override
    public String toString() {
        return "Attribute{" +
            "name='" + name + '\'' +
            ", value='" + value + '\'' +
            '}';
    }
}
