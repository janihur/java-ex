import java.util.Objects;

public class Name {
    public String firstName = null;
    public String lastName = null;

    public Name() {}

    public Name(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Name)) return false;

        Name name = (Name) obj;

        return
            Objects.equals(this.firstName, name.firstName) &&
            Objects.equals(this.lastName, name.lastName)
        ;
    }

    @Override
    public String toString() {
        return "Name{" +
            "firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            '}';
    }
}
