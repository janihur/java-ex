import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import org.junit.Test;

public class Test01 {
    @Test
    public void MatchVersionNumber1() {
	Pattern versionPattern = Pattern.compile("^([\\d.]+)");

	String[] input = new String[]{
	    "100-SNAPSHOT (version=1.0.0-SNAPSHOT)(branch=master)(buildtime=2018-11-29 08:31:30.279855)(deploytime=2018-11-29 09:30:30.931882)",
	    "1.0.0 (version=1.0.0-SNAPSHOT)(branch=master)(buildtime=2018-11-29 08:31:30.279855)(deploytime=2018-11-29 09:30:30.931882)"
	};

	{
	    Matcher m = versionPattern.matcher(input[0]);
	    assertThat("input[0] find",  m.find(),   is(true));
	    assertThat("input[0] start", m.start(),  is(0));
	    assertThat("input[0] end",   m.end(),    is(3));
	    assertThat("input[0] group", m.group(1), is("100"));
	}
	{
	    Matcher m = versionPattern.matcher(input[1]);
	    assertThat("input[1] find",  m.find(),   is(true));
	    assertThat("input[1] start", m.start(),  is(0));
	    assertThat("input[1] end",   m.end(),    is(5));
	    assertThat("input[1] group", m.group(1), is("1.0.0"));
	}
    }

    @Test
    public void MatchVersionNumber2() {
	Pattern versionPattern = Pattern.compile("^(\\d+)\\.(\\d+)\\.(\\d+)");

	String[] input = new String[]{
	    "1.2.3.SNAPSHOT",
	    "1.22.3-SNAPSHOT",
	    "1.2.3"
	};

	{
	    Matcher m = versionPattern.matcher(input[0]);
	    assertThat("input[0] find",  m.find(),   is(true));
	    assertThat("input[0] start", m.start(),  is(0));
	    assertThat("input[0] end",   m.end(),    is(5));
	    assertThat("input[0] group 0", m.group(0), is("1.2.3"));
	    assertThat("input[0] group 1", m.group(1), is("1"));
	    assertThat("input[0] group 2", m.group(2), is("2"));
	    assertThat("input[0] group 3", m.group(3), is("3"));
	}
	{
	    Matcher m = versionPattern.matcher(input[1]);
	    assertThat("input[1] find",  m.find(),   is(true));
	    assertThat("input[1] start", m.start(),  is(0));
	    assertThat("input[1] end",   m.end(),    is(6));
	    assertThat("input[1] group 0", m.group(0), is("1.22.3"));
	    assertThat("input[1] group 1", m.group(1), is("1"));
	    assertThat("input[1] group 2", m.group(2), is("22"));
	    assertThat("input[1] group 3", m.group(3), is("3"));
	}
	{
	    Matcher m = versionPattern.matcher(input[2]);
	    assertThat("input[2] find",  m.find(),   is(true));
	    assertThat("input[2] start", m.start(),  is(0));
	    assertThat("input[2] end",   m.end(),    is(5));
	    assertThat("input[2] group 0", m.group(0), is("1.2.3"));
	    assertThat("input[2] group 1", m.group(1), is("1"));
	    assertThat("input[2] group 2", m.group(2), is("2"));
	    assertThat("input[2] group 3", m.group(3), is("3"));
	}
    }

	@Test
	// replace prefix:
	// 35844... -> 044...
	//   044... -> 044...
	public void ReplaceMatchingGroup() {
		final String matchPattern = "^(358|0)(\\d+)";
		final String replacementPattern = "0$2";
		final String expected = "04412345678";

		assertThat("3584412345678".replaceAll(matchPattern, replacementPattern), is(expected));
		assertThat(  "04412345678".replaceAll(matchPattern, replacementPattern), is(expected));
	}
}
