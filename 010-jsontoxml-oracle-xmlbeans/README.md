010-jsontoxml-oracle-xmlbeans
=============================

Example how to convert between JSON and XML with in-house code. The implementation should conform W3C's [XML representation of JSON](https://www.w3.org/TR/xpath-functions/#json).

Uses Oracle proprietary library (used by Oracle proprietary system) that is not included. You might be able to replace the Oracle library with https://search.maven.org/artifact/org.apache.xmlbeans/xmlbeans/2.6.0/jar
