#!/bin/bash

# usage:
# ./run.sh j2x "$(cat example.json)"
# ./run.sh x2j "$(cat example.xml)" | jq .

declare -r JACKSON=~/.m2/repository/com/fasterxml/jackson/core/jackson-core/2.3.1/jackson-core-2.3.1.jar
# proprietary Oracle library, you might be able to replace that with
# https://search.maven.org/artifact/org.apache.xmlbeans/xmlbeans/2.6.0/jar
declare -r XMLBEANS=com.bea.core.xml.xmlbeans.jar
declare -r THIS=target/010-jsontoxml-oracle-xmlbeans-1.0.jar

java -cp "${JACKSON}:${XMLBEANS}:${THIS}" Main "$@"
