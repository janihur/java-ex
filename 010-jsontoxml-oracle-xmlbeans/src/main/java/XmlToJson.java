import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayDeque;
import java.util.Deque;

import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlCursor.TokenType;
import org.apache.xmlbeans.XmlObject;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

public class XmlToJson {

    private static final JsonFactory jfactory = new JsonFactory();

    // W3c JSON as XML element names
    public enum JsonElementName {
        ARRAY, MAP, STRING, BOOLEAN, NULL, NUMBER
    }

    public static String xmlToJson(XmlObject xml) {

        StringWriter writer = new StringWriter();
        JsonGenerator generator = null;
        try {
            generator = jfactory.createGenerator(writer);

            // https://xmlbeans.apache.org/docs/2.6.0/reference/index.html?org/apache/xmlbeans/XmlCursor.html
            XmlCursor cursor = null;
            try {
                cursor = xml.newCursor();
                cursor.toStartDoc();

                // store current element name, text and key attribute value
                JsonElementName currentElement = null;
                String currentKey = null;
                String currentText = null;
                boolean readText = false;

                // keep track of opening/closing of elements
                Deque<JsonElementName> currentPath = new ArrayDeque<>();

                while (cursor.hasNextToken()) {
                    cursor.toNextToken();

                    // process parent/sibling element in the beginning of the child/sibling
                    // thus write for example { in the beginning of first child element of map
                    // element

                    final TokenType currentTokenType = cursor.currentTokenType();

                    // System.err.println("---");
                    // System.err.println("* currentTokenType: " + currentTokenType);
                    // System.err.println("- (begin of iteration)");
                    // System.err.println("* currentElement: " + currentElement);
                    // System.err.println("* currentKey: " + currentKey);
                    // System.err.println("* currentText: " + currentText);
                    // System.err.println("* readText: " + readText);

                    if (currentElement != null
                        && (currentTokenType == TokenType.START || currentTokenType == TokenType.ENDDOC)) {

                        if (currentElement == JsonElementName.MAP) {
                            if (currentKey != null) {
                                generator.writeFieldName(currentKey);
                            }

                            generator.writeStartObject();

                        } else if (currentElement == JsonElementName.STRING) {
                            if (currentKey != null) {
                                generator.writeFieldName(currentKey);
                            }
                            generator.writeString(currentText);
                        } else if (currentElement == JsonElementName.BOOLEAN) {
                            if (currentKey != null) {
                                generator.writeFieldName(currentKey);
                            }
                            generator.writeBoolean(Boolean.valueOf(currentText));
                        }

                        else if (currentElement == JsonElementName.NULL) {
                            if (currentKey != null) {
                                generator.writeFieldName(currentKey);
                            }
                            generator.writeNull();
                        }

                        else if (currentElement == JsonElementName.ARRAY) {
                            if (currentKey != null) {
                                generator.writeFieldName(currentKey);
                            }
                            generator.writeStartArray();

                        }

                        else if (currentElement == JsonElementName.NUMBER) {
                            if (currentKey != null) {
                                generator.writeFieldName(currentKey);
                            }
                            generator.writeNumber(currentText);
                        }

                        else {
                            throw new RuntimeException(currentElement + "");
                        }

                        // don't allow reusing values for another element
                        currentElement = null;
                        currentText = null;
                        currentKey = null;
                    }

                    if (currentTokenType == TokenType.START) {

                        // ignore name space for simplicity
                        String el = cursor.getName().getLocalPart();

                        if ("array".equals(el)) {
                            currentElement = JsonElementName.ARRAY;
                        } else if ("map".equals(el)) {
                            currentElement = JsonElementName.MAP;
                        } else if ("string".equals(el)) {
                            currentElement = JsonElementName.STRING;
                            readText = true;
                        } else if ("boolean".equals(el)) {
                            currentElement = JsonElementName.BOOLEAN;
                            readText = true;
                        } else if ("null".equals(el)) {
                            currentElement = JsonElementName.NULL;
                        } else if ("number".equals(el)) {
                            currentElement = JsonElementName.NUMBER;
                            readText = true;
                        }
                        else {
                            throw new RuntimeException("Illegal el '" + el + "'");
                        }

                        currentPath.push(currentElement);

                    } else if (currentTokenType == TokenType.END) {

                        // if END, we can write corresponding element
                        if (currentElement != null) {
                            // duplicated block from upper
                            if (currentElement == JsonElementName.MAP) {
                                if (currentKey != null) {
                                    generator.writeFieldName(currentKey);
                                }
                                generator.writeStartObject();
                            } else if (currentElement == JsonElementName.STRING) {
                                if (currentKey != null) {
                                    generator.writeFieldName(currentKey);
                                }
                                // System.err.println("** (END) currentText: " + currentText);
                                // empty string is "", not null [DIL-8975]
                                generator.writeString(currentText != null ? currentText : "");
                            } else if (currentElement == JsonElementName.BOOLEAN) {
                                if (currentKey != null) {
                                    generator.writeFieldName(currentKey);
                                }
                                generator.writeBoolean(Boolean.valueOf(currentText));
                            }
                            else if (currentElement == JsonElementName.NULL) {
                                if (currentKey != null) {
                                    generator.writeFieldName(currentKey);
                                }
                                generator.writeNull();
                            }
                            else if (currentElement == JsonElementName.ARRAY) {
                                if (currentKey != null) {
                                    generator.writeFieldName(currentKey);
                                }
                                generator.writeStartArray();
                            }
                            else if (currentElement == JsonElementName.NUMBER) {
                                if (currentKey != null) {
                                    generator.writeFieldName(currentKey);
                                }
                                generator.writeNumber(currentText);
                            }
                            else {
                                throw new RuntimeException(currentElement + "");
                            }

                            // don't allow reusing values for another element
                            currentElement = null;
                            currentText = null;
                            currentKey = null;
                        }

                        // if we are closing map or array
                        JsonElementName lastClosingTag = currentPath.pop();

                        if (lastClosingTag == JsonElementName.ARRAY) {
                            generator.writeEndArray();
                        } else if (lastClosingTag == JsonElementName.MAP) {
                            generator.writeEndObject();
                        }

                    }

                    else if (currentTokenType == TokenType.ATTR) {
                        // [DIL-8975]
                        currentText = null;
                        String attrName = cursor.getName().getLocalPart();
                        // should only have key attribute
                        if ("key".equals(attrName)) {
                            currentKey = cursor.getTextValue();
                        } else {
                            throw new RuntimeException("Illegal attrName '" + attrName + "'");
                        }

                    } else if (currentTokenType == TokenType.TEXT) {
                        if (readText) {
                            currentText = cursor.getTextValue();
                            readText = false;
                        }
                    }

                    // System.err.println("- (end of iteration)");
                    // System.err.println("* currentElement: " + currentElement);
                    // System.err.println("* currentKey: " + currentKey);
                    // System.err.println("* currentText: " + currentText);
                    // System.err.println("* readText: " + readText);
                }

            } finally {
                if (cursor != null) {
                    cursor.dispose();
                }
            }

            generator.close();
            return writer.toString();

        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                writer.close();
            } catch (IOException e) {
                // ignore
            }
        }

    }
}