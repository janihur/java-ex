import org.apache.xmlbeans.XmlObject;

public class Main {
    public static void main(String[] args) {
        switch (args[0]) {
            case "j2x":
                try {
                    XmlObject xml = JsonToXml.jsonToXml(args[1]);
                    System.out.println(xml.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case "x2j":
                try {
                    XmlObject xml = XmlObject.Factory.parse(args[1]);
                    System.out.println(XmlToJson.xmlToJson(xml));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                System.err.println("invalid first parameter");
        }
    }
}
