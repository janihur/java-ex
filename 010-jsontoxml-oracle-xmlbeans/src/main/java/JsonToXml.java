import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlObject;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

public class JsonToXml {

    private static final String HTTP_WWW_W3_ORG_2005_XPATH_FUNCTIONS = "http://www.w3.org/2005/xpath-functions";
    private static final String ATTR_NAME_KEY = "key";
    final static JsonFactory jfactory;

    static {
        jfactory = new JsonFactory();
    }

    public static XmlObject jsonToXml(String json) throws Exception {

        JsonParser parser = jfactory.createParser(json);
        XmlObject writer = XmlObject.Factory.newInstance();
        XmlCursor cursor = null;

        try {
            String fieldName = null;

            boolean nsWrote = false;

            cursor = writer.newCursor();
            cursor.toNextToken();

            while (parser.nextToken() != null) {

                JsonToken currentToken = parser.getCurrentToken();

                if (currentToken == JsonToken.START_OBJECT) {
                    cursor.push();
                    cursor.beginElement("map", HTTP_WWW_W3_ORG_2005_XPATH_FUNCTIONS);

                    if (!nsWrote) {
                        cursor.insertNamespace("", HTTP_WWW_W3_ORG_2005_XPATH_FUNCTIONS);
                        nsWrote = true;
                    }

                    if (fieldName != null) {
                        cursor.insertAttributeWithValue(ATTR_NAME_KEY, fieldName);
                    }
                }

                else if (currentToken == JsonToken.END_OBJECT) {
                    cursor.pop();
                }

                else if (currentToken == JsonToken.FIELD_NAME) {
                    fieldName = parser.getCurrentName();
                }

                else if (currentToken == JsonToken.VALUE_TRUE || currentToken == JsonToken.VALUE_FALSE) {
                    handleFieldOrArrayItem(parser, fieldName, "boolean", cursor);
                }

                else if (currentToken == JsonToken.VALUE_NULL) {

                    if (fieldName == null) {
                        cursor.push();
                        cursor.beginElement("null", HTTP_WWW_W3_ORG_2005_XPATH_FUNCTIONS);
                        cursor.pop();

                    } else {
                        cursor.push();
                        cursor.beginElement("null", HTTP_WWW_W3_ORG_2005_XPATH_FUNCTIONS);
                        cursor.insertAttributeWithValue(ATTR_NAME_KEY, fieldName);
                        cursor.pop();
                        // don't allow reuse
                        fieldName = null;
                    }

                }

                else if (currentToken == JsonToken.VALUE_NUMBER_INT) {
                    handleFieldOrArrayItem(parser, fieldName, "number", cursor);
                    // don't allow reuse
                    fieldName = null;
                }

                else if (currentToken == JsonToken.VALUE_NUMBER_FLOAT) {
                    handleFieldOrArrayItem(parser, fieldName, "number", cursor);
                    // don't allow reuse
                    fieldName = null;
                }

                else if (currentToken == JsonToken.VALUE_STRING) {

                    handleFieldOrArrayItem(parser, fieldName, "string", cursor);
                    // don't allow reuse
                    fieldName = null;

                } else if (currentToken == JsonToken.END_ARRAY) {
                    cursor.pop();
                }

                else if (currentToken == JsonToken.START_ARRAY) {
                    cursor.push();
                    cursor.beginElement("array", HTTP_WWW_W3_ORG_2005_XPATH_FUNCTIONS);
                    if (fieldName != null) {
                        cursor.insertAttributeWithValue(ATTR_NAME_KEY, fieldName);
                        // don't allow reuse
                        fieldName = null;
                    }
                }

                else {
                    throw new UnsupportedOperationException("Unsupported token:" + parser.getCurrentToken());
                }

            }
        } finally {
            if (cursor != null) {
                cursor.dispose();
            }
        }

        return writer;

    }

    private static void handleFieldOrArrayItem(JsonParser parser, String fieldName, String typeName, XmlCursor cursor)
        throws Exception {
        if (fieldName == null) {
            cursor.push();
            cursor.beginElement(typeName, HTTP_WWW_W3_ORG_2005_XPATH_FUNCTIONS);
            cursor.insertChars(parser.getText());
            cursor.pop();
        } else {
            cursor.push();
            cursor.beginElement(typeName, HTTP_WWW_W3_ORG_2005_XPATH_FUNCTIONS);
            cursor.insertAttributeWithValue(ATTR_NAME_KEY, fieldName);
            cursor.insertChars(parser.getText());
            cursor.pop();

        }
    }

}