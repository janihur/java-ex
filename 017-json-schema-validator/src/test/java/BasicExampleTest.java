import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaFactory;
import com.networknt.schema.SpecVersion;
import com.networknt.schema.ValidationMessage;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.fail;

public class BasicExampleTest {
    static private JsonSchema jsonSchema;

    @BeforeAll
    static public void beforeAll() {
        JsonSchemaFactory jsonSchemaFactory = JsonSchemaFactory.getInstance(SpecVersion.VersionFlag.V202012);
        InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("basicExample/schema.json");
        BasicExampleTest.jsonSchema = jsonSchemaFactory.getSchema(inputStream);
        // System.err.println(jsonSchema.toString());
    }

    @Test
    @DisplayName("Basic example: valid JSON object")
    // http://json-schema.org/learn/miscellaneous-examples.html#basic
    public void basicExample01() {
        JsonNode jsonNode = null;
        try {
            InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("basicExample/person-object-01.json");
            ObjectMapper mapper = new ObjectMapper();
            jsonNode = mapper.readTree(inputStream);
        } catch (IOException ex) {
            fail("Unexpected exception in Jackson ObjectMapper", ex);
        }

        // System.err.println(jsonNode.toString());

        Set<ValidationMessage> errors = BasicExampleTest.jsonSchema.validate(jsonNode);

        assertThat("no errors", errors.size(), is(0));
    }

    @Test
    @DisplayName("Basic example: invalid JSON object with one problem")
    // http://json-schema.org/learn/miscellaneous-examples.html#basic
    public void basicExample02() {
        JsonNode jsonNode = null;
        try {
            InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("basicExample/person-object-02.json");
            ObjectMapper mapper = new ObjectMapper();
            jsonNode = mapper.readTree(inputStream);
        } catch (IOException ex) {
            fail("Unexpected exception in Jackson ObjectMapper", ex);
        }

        // System.err.println(jsonNode.toString());

        Set<ValidationMessage> errors = BasicExampleTest.jsonSchema.validate(jsonNode);

//        for (ValidationMessage message: errors) {
//            System.err.println(message.toString());
//        }

        assertThat("one error", errors.size(), is(1));
    }
    @Test
    @DisplayName("Basic example: invalid JSON object with two problems")
    // http://json-schema.org/learn/miscellaneous-examples.html#basic
    public void basicExample03() {
        JsonNode jsonNode = null;
        try {
            InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("basicExample/person-object-03.json");
            ObjectMapper mapper = new ObjectMapper();
            jsonNode = mapper.readTree(inputStream);
        } catch (IOException ex) {
            fail("Unexpected exception in Jackson ObjectMapper", ex);
        }

        // System.err.println(jsonNode.toString());

        Set<ValidationMessage> errors = BasicExampleTest.jsonSchema.validate(jsonNode);

//        for (ValidationMessage message: errors) {
//            System.err.println(message.toString());
//        }

        assertThat("one error", errors.size(), is(2));
    }
}
