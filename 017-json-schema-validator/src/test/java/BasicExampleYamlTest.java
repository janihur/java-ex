import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mservicetech.openapi.common.RequestEntity;
import com.mservicetech.openapi.common.Status;
import com.mservicetech.openapi.validation.OpenApiValidator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.*;

public class BasicExampleYamlTest {
    @Test
    @DisplayName("TODO")
    public void basicExample01() {
        OpenApiValidator openApiValidator;
        {
            InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("basicExampleYaml/openapi-spec.yaml");
            openApiValidator = new OpenApiValidator(inputStream);
        }

        RequestEntity requestEntity = new RequestEntity();
        requestEntity.setContentType("application/json");
        try {
            InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("basicExampleYaml/person-object-01.json");
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(inputStream);
            requestEntity.setRequestBody(jsonNode);
        } catch (IOException ex) {
            fail("Unexpected exception in Jackson ObjectMapper", ex);
        }

        Status status = openApiValidator.validateRequestPath("/person", "get", requestEntity);

        System.err.println(status);

        assertNull(status);
    }

    @Test
    @DisplayName("TODO")
    public void basicExample02() {
        OpenApiValidator openApiValidator;
        {
            InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("basicExampleYaml/openapi-spec.yaml");
            openApiValidator = new OpenApiValidator(inputStream);
        }

        RequestEntity requestEntity = new RequestEntity();
        requestEntity.setContentType("application/json");
        try {
            InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("basicExampleYaml/person-object-02.json");
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(inputStream);
            requestEntity.setRequestBody(jsonNode);
        } catch (IOException ex) {
            fail("Unexpected exception in Jackson ObjectMapper", ex);
        }

        Status status = openApiValidator.validateRequestPath("/person", "get", requestEntity);

        System.err.println(status);

        assertNotNull(status);
    }
}
