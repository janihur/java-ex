import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

class CalculatorTest {
    @Test
    @DisplayName("1 + 1 = 2")
    void addsTwoNumbers() {
        Calculator calculator = new Calculator();
        assertEquals(2, calculator.add(1, 1), "1 + 1 should equal 2");
    }

    @ParameterizedTest(name = "add: {0} + {1} = {2}")
    @CsvSource({
            "0,    1,   1",
            "1,    2,   3",
            "49,  51, 100",
            "1,  100, 101"
    })
    void add(int first, int second, int expectedResult) {
        Calculator calculator = new Calculator();
        assertEquals(
            expectedResult,
            calculator.add(first, second),
            () -> first + " + " + second + " should equal " + expectedResult
        );
    }

    static Stream<Arguments> mulTestCases() {
        return Stream.of(
            Arguments.of(0,    1,    0),
            Arguments.of(1,    2,    2),
            Arguments.of(49,  51, 2499),
            Arguments.of(1,  100,  100)
        );
    }

    @ParameterizedTest(name = "mul: {index} ==> {0} * {1} = {2}")
    @MethodSource("mulTestCases")
    void mul(int first, int second, int expectedResult) {
        Calculator calculator = new Calculator();
        assertEquals(
            expectedResult,
            calculator.mul(first, second),
            () -> first + " * " + second + " should equal " + expectedResult
        );
    }

    @Test
    @DisplayName("Hamcrest assertion example #1")
    void hamcrestAddTwoNumbers() {
        Calculator calculator = new Calculator();
        assertThat("1 + 1 should equal 2", calculator.add(1, 1), is(2));
    }
}