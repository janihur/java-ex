import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collector;

public class ImmutableListCollector {

    public static <t> Collector<t, List<t>, List<t>> toImmutableList() {
        return Collector.of(
            /* supplier */ ArrayList::new, 
            /* accumulator */ List::add, 
            /* combiner */ (a, b) -> {
                a.addAll(b);
                return a;
            },
            /* finisher */ Collections::unmodifiableList
        );
    }
}