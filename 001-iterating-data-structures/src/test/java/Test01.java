import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.management.ObjectName;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class Test01 {
    private static final int ARRAY_SIZE = 100;
    private static Object[] objectNames = new Object[ARRAY_SIZE];

    private static void assert_(List<String> actual) {
        assertThat("list size", 99, is(99));
        assertThat("item 0",  actual.get(0), is("net.jani-hur:Name=Name0"));
        assertThat("item 68", actual.get(68), is("net.jani-hur:Name=Name68"));
        assertThat("item 69", actual.get(69), is("net.jani-hur:Name=Name70"));
        assertThat("item 98", actual.get(98), is("net.jani-hur:Name=Name99"));
    }

    @BeforeAll
    public static void testDataInit() throws javax.management.MalformedObjectNameException {
        for (int i = 0; i < ARRAY_SIZE; i++) {
            if (69 == i) {
                objectNames[i] = (Object)new String("KABOOM!");
            } else {
                objectNames[i] = (Object)new ObjectName("net.jani-hur", "Name", "Name" + i);
            }
        }
    }

    @Test
    public void enhancedForLoop() {
        List<String> result = new ArrayList<String>();

        for (Object o : objectNames) {
            if (o instanceof ObjectName ) {
                result.add(((ObjectName)o).getCanonicalName());
            }
        }

        assert_(result);
    }

    @Test
    public void streamForEach() {
        List<String> result = new ArrayList<String>();

        Arrays.stream(objectNames)
            .forEach(o -> {
                if (o instanceof ObjectName ) {
                    result.add(((ObjectName)o).getCanonicalName());
                }
            });

        assert_(result);
    }

    @Test
    public void streamMapStandardCollector() {
        List<String> result = Arrays.stream(objectNames)
            .filter(o -> o instanceof ObjectName)
            .map(o -> (ObjectName) o)
            .map(ObjectName::getCanonicalName)
            .collect(Collectors.toList());

        assert_(result);
    }

    @Test
    public void streamMapCustomCollector() {
        List<String> result = Arrays.stream(objectNames)
            .filter(o -> o instanceof ObjectName)
            .map(o -> (ObjectName) o)
            .map(ObjectName::getCanonicalName)
            .collect(ImmutableListCollector.toImmutableList());

        assert_(result);
    }

}