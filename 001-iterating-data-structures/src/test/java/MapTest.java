import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class MapTest {
    @Test
    @DisplayName("Change key case.")
    public void test01() {
        // uppercase keys -----------------------------------------------------
        Map<String, Object> m = new HashMap<>();
        m.put("FOO_FOO", 1);
        m.put("BAR_BAR", "lorem ipsum");

        // --------------------------------------------------------------------

        assertNull(m.get("foo_foo"));
        assertNull(m.get("bar_bar"));

        assertEquals(1, m.get("FOO_FOO"));
        assertEquals("lorem ipsum", m.get("BAR_BAR"));

        // lowercase keys -----------------------------------------------------

        Set<String> oldKeys = new HashSet<>(m.keySet());

        for (String oldKey: oldKeys) {
            String newKey = oldKey.toLowerCase();
            m.put(newKey, m.remove(oldKey));
        }

        // --------------------------------------------------------------------

        assertNull(m.get("FOO_FOO"));
        assertNull(m.get("BAR_BAR"));

        assertEquals(1, m.get("foo_foo"));
        assertEquals("lorem ipsum", m.get("bar_bar"));
    }
}
