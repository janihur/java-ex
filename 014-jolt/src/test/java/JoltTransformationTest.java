import com.bazaarvoice.jolt.Chainr;
import com.bazaarvoice.jolt.Diffy;
import com.bazaarvoice.jolt.JsonUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class JoltTransformationTest {
    // https://github.com/bazaarvoice/jolt/blob/master/gettingStarted.md
    @Test
    @DisplayName("Getting Started")
    public void test01() {
        List chainrSpecJSON = JsonUtils.classpathToList("/test01/spec.json");
        Chainr chainr = Chainr.fromSpec(chainrSpecJSON);

        Object inputJSON = JsonUtils.classpathToObject("/test01/input.json");

        Object transformedOutput = chainr.transform(inputJSON);

        // System.out.println(JsonUtils.toPrettyJsonString(transformedOutput));

        Diffy.Result r = diff(JsonUtils.classpathToObject("/test01/expected.json"), transformedOutput);
        assertThat(r.toString(), r.isEmpty(), is(true));
    }

    @Test
    @DisplayName("Transform DSL: remove")
    public void test02() {
        Object expected = JsonUtils.classpathToObject("/test02/expected.json");
        Object actual = transform("/test02/spec.json", "/test02/input.json");//chainr.transform(input);
        // System.out.println(JsonUtils.toPrettyJsonString(actual));
        Diffy.Result r = diff(expected, actual);
        assertThat(r.toString(), r.isEmpty(), is(true));
    }

    @Test
    @DisplayName("Transform DSL: shift: iterate JSON array")
    public void test03() {
        Object expected = JsonUtils.classpathToObject("/test03/expected.json");
        Object actual = transform("/test03/spec.json", "/test03/input.json");
        // System.out.println(JsonUtils.toPrettyJsonString(actual));
        Diffy.Result r = diff(expected, actual);
        assertThat(r.toString(), r.isEmpty(), is(true));
    }

    @Test
    @DisplayName("Transform DSL: type conversions and trimming")
    public void test04() {
        Object expected = JsonUtils.classpathToObject("/test04/expected.json");
        Object actual = transform("/test04/spec.json", "/test04/input.json");
        // System.out.println(JsonUtils.toPrettyJsonString(actual));
        Diffy.Result r = diff(expected, actual);
        assertThat(r.toString(), r.isEmpty(), is(true));
    }

    @Test
    @DisplayName("Transform DSL: conditional with value matching")
    public void test05() {
        Object expected = JsonUtils.classpathToObject("/test05/expected.json");
        Object actual = transform("/test05/spec.json", "/test05/input.json");
        // System.out.println(JsonUtils.toPrettyJsonString(actual));
        Diffy.Result r = diff(expected, actual);
        assertThat(r.toString(), r.isEmpty(), is(true));
    }

    @Test
    @DisplayName("Transform DSL: conditional with value matching II")
    public void test06() {
        Object expected = JsonUtils.classpathToObject("/test06/expected.json");
        Object actual = transform("/test06/spec.json", "/test06/input.json");
        // System.out.println(JsonUtils.toPrettyJsonString(actual));
        Diffy.Result r = diff(expected, actual);
        assertThat(r.toString(), r.isEmpty(), is(true));
    }

    @Test
    @DisplayName("Transform DSL: pass extra data to transformation with context")
    public void test07() {
        Object expected = JsonUtils.classpathToObject("/test07/expected.json");
        Object actual = transform(
        "/test07/spec.json",
        "/test07/input.json",
            Map.of(
                "BAR_VALUE", "This is BAR! (value from context)",
                "BAR_KEY", "bar",
                // set works too but the result can't be asserted as the element order is not guaranteed
                // "ZOO_SET", Set.of("ZOO_SET_1", "ZOO_SET_2"),
                "ZOO_LIST", List.of("ZOO_LIST_1", "ZOO_LIST_2"),
                "ZOO_LIST_EMPTY", List.of()
            )
        );
        // System.out.println(JsonUtils.toPrettyJsonString(actual));
        Diffy.Result r = diff(expected, actual);
        assertThat(r.toString(), r.isEmpty(), is(true));
    }

    @Test
    @DisplayName("Transform DSL: transform {key: K1, value: V1} to {K1: V1}")
    public void test08() {
        Object expected = JsonUtils.classpathToObject("/test08/expected.json");
        Object actual = transform("/test08/spec.json","/test08/input.json");
        // System.out.println(JsonUtils.toPrettyJsonString(actual));
        Diffy.Result r = diff(expected, actual);
        assertThat(r.toString(), r.isEmpty(), is(true));
    }

    @Test
    @DisplayName("Transform DSL: transform a literal value to boolean")
    public void test09() {
        Object expected = JsonUtils.classpathToObject("/test09/expected.json");
        Object actual = transform("/test09/spec.json","/test09/input.json");
        // System.out.println(JsonUtils.toPrettyJsonString(actual));
        Diffy.Result r = diff(expected, actual);
        assertThat(r.toString(), r.isEmpty(), is(true));
    }

    @Test
    @DisplayName("Transform DSL: empty input with context")
    public void test10() {
        Object expected = JsonUtils.classpathToObject("/test10/expected.json");
        Map<String, Object> context = Map.of(
            "ID", 123,
            "FOO", "this is foo",
            "BAR", "this is bar"
        );

        Chainr chainr = Chainr.fromSpec(JsonUtils.classpathToList("/test10/spec.json"));
        Object input = JsonUtils.jsonToObject("{}"); // minimal valid value
        Object actual = chainr.transform(input, context);

        // System.out.println(JsonUtils.toPrettyJsonString(actual));
        Diffy.Result r = diff(expected, actual);
        assertThat(r.toString(), r.isEmpty(), is(true));
    }

    @Test
    @DisplayName("Transform DSL: Map ids")
    public void test11() {
        Object expected = JsonUtils.classpathToObject("/test11/expected.json");
        Object actual = transform("/test11/spec.json","/test11/input.json");
        // System.out.println(JsonUtils.toPrettyJsonString(actual));
        Diffy.Result r = diff(expected, actual);
        assertThat(r.toString(), r.isEmpty(), is(true));
    }

    @Test
    @DisplayName("Transform DSL: from { K1: V1, K2: V2 } to [{ key: K1, value: V1 }, { key: K2, value: V2 }]")
    public void test12() {
        runTest(true, false);
    }

    @Test
    @DisplayName("Transform DSL: same than test12 but with arrays")
    public void test13() {
        runTest(true, false);
    }

    private void runTest(boolean assert_, boolean print) {
        String testName = // equals to test method name
            StackWalker.getInstance()
            .walk(s -> s.skip(1).findFirst())
            .get()
            .getMethodName()
        ;

        Object expected = JsonUtils.classpathToObject("/" + testName + "/expected.json");
        Object actual = transform("/" + testName + "/spec.json","/" + testName + "/input.json");
        if (print) {
            System.out.println(JsonUtils.toPrettyJsonString(actual));
        }
        if (assert_) {
            Diffy.Result r = diff(expected, actual);
            assertThat(r.toString(), r.isEmpty(), is(true));
        }
    }

    private Object transform(String specPath, String inputPath) {
        Chainr chainr = Chainr.fromSpec(JsonUtils.classpathToList(specPath));
        Object input = JsonUtils.classpathToObject(inputPath);
        Object transformed = chainr.transform(input);
        return transformed;
    }

    private Object transform(String specPath, String inputPath, Map<String, Object> context) {
        Chainr chainr = Chainr.fromSpec(JsonUtils.classpathToList(specPath));
        Object input = JsonUtils.classpathToObject(inputPath);
        Object transformed = chainr.transform(input, context);
        return transformed;
    }

    private Diffy.Result diff(Object expected, Object actual) {
        Diffy diffy = new Diffy();
        Diffy.Result r = diffy.diff(expected, actual);
        return r;
    }
}
