import java.util.*;

public class Main {
    final private static String ROW_SEPARATOR = "<>";

    public static List<List<String>> StringArrayToMatrix(String[] strArr) {
        List<List<String>> matrix = new ArrayList<List<String>>();

        if (strArr == null) {
            return matrix;
        }

        List<String> row = new ArrayList<String>();

        for (String s: strArr) {
            if (s.equals(ROW_SEPARATOR)) {
                if (row.size() > 0) {
                    matrix.add(row);
                    row = new ArrayList<String>();
                }
            } else {
                row.add(s);
            }
        }

        if (row.size() > 0) {
            matrix.add(row);
        }

        return matrix;
    }

    public static Boolean IsSquare(List<List<String>> matrix) {
        final Integer MATRIX_SIZE = matrix.size();

        if (MATRIX_SIZE == 0) {
            return false;
        }

        for (List<String> row: matrix) {
            if (row.size() != MATRIX_SIZE) {
                return false;
            }
        }

        return true;
    }

    public static Boolean IsSymmetric(List<List<String>> matrix) {
        final Integer SIZE = matrix.size();

        for (Integer y = 0; y < SIZE; y++) {
            for (Integer x = 0; x < SIZE; x++) {
                if (y == x) {
                    continue; // skips the main diagonal
                }
                final String value           = matrix.get(y).get(x);
                final String valueTransposed = matrix.get(x).get(y);
                if (!value.equals(valueTransposed)) {
                    return false;
                }
            }
        }

        return true;
    }

    public static String MatrixChallenge(String[] strArr) {
        List<List<String>> matrix = StringArrayToMatrix(strArr);

        if (!IsSquare(matrix)) {
            return "not possible";
        }

        return IsSymmetric(matrix) ? "symmetric" : "not symmetric";
    }
}
