import java.util.*;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Test01 {
    @Test
    public void StringArrayToMatrixTest() {
        {
            List<List<String>> expected = new ArrayList<List<String>>();
            expected.add(Arrays.asList(new String[]{"5", "0"}));
            expected.add(Arrays.asList(new String[]{"0", "5"}));

            assertEquals(
                expected,
                Main.StringArrayToMatrix(new String[]{"5", "0", "<>", "0", "5"})
            );
        }
        {
            List<List<String>> expected = new ArrayList<List<String>>();
            expected.add(Arrays.asList(new String[]{"1", "2", "4"}));
            expected.add(Arrays.asList(new String[]{"2", "1", "1"}));
            expected.add(Arrays.asList(new String[]{"-4", "1", "-1"}));

            assertEquals(
                expected,
                Main.StringArrayToMatrix(new String[]{"1", "2", "4", "<>", "2", "1", "1", "<>", "-4", "1", "-1"})
            );
        }
        {
            List<List<String>> expected = new ArrayList<List<String>>();
            expected.add(Arrays.asList(new String[]{"5", "0"}));
            expected.add(Arrays.asList(new String[]{"0", "5"}));

            assertEquals(
                expected,
                Main.StringArrayToMatrix(new String[]{"5", "0", "<>", "0", "5", "<>"})
            );
        }
        {
            List<List<String>> expected = new ArrayList<List<String>>();
            expected.add(Arrays.asList(new String[]{"5", "0"}));
            expected.add(Arrays.asList(new String[]{"0", "5"}));

            assertEquals(
                expected,
                Main.StringArrayToMatrix(new String[]{"5", "0", "<>", "<>", "0", "5", "<>"})
            );
        }
        {
            List<List<String>> expected = new ArrayList<List<String>>();

            assertEquals(
                expected,
                Main.StringArrayToMatrix(new String[]{"<>"})
            );
        }
        {
            List<List<String>> expected = new ArrayList<List<String>>();

            assertEquals(
                expected,
                Main.StringArrayToMatrix(new String[]{})
            );
        }
        {
            List<List<String>> expected = new ArrayList<List<String>>();

            assertEquals(
                expected,
                Main.StringArrayToMatrix(null)
            );
        }
       }

    @Test
    public void IsSquareTest() {
        {
            List<List<String>> matrix = new ArrayList<List<String>>();
            matrix.add(Arrays.asList(new String[]{"0"}));

            assertEquals(true, Main.IsSquare(matrix));
        }
        {
            List<List<String>> matrix = new ArrayList<List<String>>();
            matrix.add(Arrays.asList(new String[]{"5", "0"}));
            matrix.add(Arrays.asList(new String[]{"0", "5"}));

            assertEquals(true, Main.IsSquare(matrix));
        }
        {
            List<List<String>> matrix = new ArrayList<List<String>>();
            matrix.add(Arrays.asList(new String[]{}));

            assertEquals(false, Main.IsSquare(matrix));
        }
        {
            List<List<String>> matrix = new ArrayList<List<String>>();

            assertEquals(false, Main.IsSquare(matrix));
        }
        {
            List<List<String>> matrix = new ArrayList<List<String>>();
            matrix.add(Arrays.asList(new String[]{"5", "0"}));
            matrix.add(Arrays.asList(new String[]{"0"}));

            assertEquals(false, Main.IsSquare(matrix));
        }
        {
            List<List<String>> matrix = new ArrayList<List<String>>();
            matrix.add(Arrays.asList(new String[]{"0"}));
            matrix.add(Arrays.asList(new String[]{"0", "5"}));

            assertEquals(false, Main.IsSquare(matrix));
        }
    }

    @Test
    public void IsSymmetricTest() {
        {
            List<List<String>> matrix = new ArrayList<List<String>>();
            matrix.add(Arrays.asList(new String[]{"5", "0"}));
            matrix.add(Arrays.asList(new String[]{"0", "5"}));

            assertEquals(true, Main.IsSymmetric(matrix));
        }
        {
            List<List<String>> matrix = new ArrayList<List<String>>();
            matrix.add(Arrays.asList(new String[]{"1", "7", "3"}));
            matrix.add(Arrays.asList(new String[]{"7", "4", "5"}));
            matrix.add(Arrays.asList(new String[]{"3", "5", "1"}));

            assertEquals(true, Main.IsSymmetric(matrix));
        }
        {
            List<List<String>> matrix = new ArrayList<List<String>>();
            matrix.add(Arrays.asList(new String[]{"5", "0"}));
            matrix.add(Arrays.asList(new String[]{"1", "5"}));

            assertEquals(false, Main.IsSymmetric(matrix));
        }
    }

    @Test
    public void MatrixChallengeTest() {
        assertEquals("symmetric", Main.MatrixChallenge(new String[]{"1"}));
        assertEquals("symmetric", Main.MatrixChallenge(new String[]{"5", "0", "<>", "0", "5"}));
        assertEquals("not symmetric", Main.MatrixChallenge(new String[]{"1","2","4","<>","2","1","1","<>","-4","1","-1"}));
        assertEquals("not possible", Main.MatrixChallenge(new String[]{"5", "0", "<>", "0", "5", "<>", "0"}));
        assertEquals("not possible", Main.MatrixChallenge(new String[]{}));
        assertEquals("not possible", Main.MatrixChallenge(new String[]{"<>"}));
        assertEquals("not possible", Main.MatrixChallenge(null));
    }
}
