import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import net.sf.saxon.s9api.SaxonApiException;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Vector;

public class Test01 {
    @Test
    public void xml2Json() {
        InputStream xsltStylesheet = Main.class.getClassLoader().getResourceAsStream("xml-to-json.xsl");
        InputStream input = Main.class.getClassLoader().getResourceAsStream("input-01.xml");

        try {
            SaxonXslt xslt = new SaxonXslt(xsltStylesheet);
            String output = xslt.transform(input);
            System.err.println(output);
        } catch (SaxonApiException ex) {
            ex.printStackTrace();
            fail("Saxon exception");
        }
    }

    @Test
    public void json2Xml() {
        InputStream xsltStylesheet = Main.class.getClassLoader().getResourceAsStream("json-to-xml.xsl");
        InputStream inputBody = Main.class.getClassLoader().getResourceAsStream("input-01.json");

        String inputPrefix = "<json><![CDATA[";
        String inputPostfix = "]]></json>";

        Vector<InputStream> inputs = new Vector<>();
        inputs.add(new ByteArrayInputStream(inputPrefix.getBytes(StandardCharsets.UTF_8)));
        inputs.add(inputBody);
        inputs.add(new ByteArrayInputStream(inputPostfix.getBytes(StandardCharsets.UTF_8)));

        SequenceInputStream input = new SequenceInputStream(inputs.elements());

        try {
            SaxonXslt xslt = new SaxonXslt(xsltStylesheet);
            String output = xslt.transform(input);
            System.err.println(output);
        } catch (SaxonApiException ex) {
            ex.printStackTrace();
            fail("Saxon exception");
        }
    }
}
