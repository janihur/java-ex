import net.sf.saxon.s9api.*;

import javax.xml.transform.stream.StreamSource;
import java.io.InputStream;
import java.io.StringWriter;

public class Main {
    public static void main(String[] args) {
        InputStream xsltFile = Main.class.getClassLoader().getResourceAsStream("xml-to-json.xsl");
        InputStream inputFile = Main.class.getClassLoader().getResourceAsStream("input-01.xml");

        try {
            Processor processor = new Processor(false);
            XsltCompiler compiler = processor.newXsltCompiler();
            XsltExecutable stylesheet = compiler.compile(new StreamSource(xsltFile));

            StringWriter outputString = new StringWriter();
            Serializer output = processor.newSerializer(outputString);
            // output.setOutputProperty(Serializer.Property.METHOD, "text");

            Xslt30Transformer transformer = stylesheet.load30();

            transformer.transform(new StreamSource(inputFile), output);

            System.out.println(outputString.toString());
        } catch (SaxonApiException ex) {
            System.err.println("Unexpected exception:");
            ex.printStackTrace();
        }
    }
}
