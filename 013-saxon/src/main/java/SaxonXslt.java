import net.sf.saxon.s9api.*;

import javax.xml.transform.stream.StreamSource;
import java.io.InputStream;
import java.io.StringWriter;

public class SaxonXslt {
    public SaxonXslt(InputStream xsltStylesheet) throws SaxonApiException {
        this.xsltCompiler = this.processor.newXsltCompiler();
        this.stylesheet = this.xsltCompiler.compile(new StreamSource(xsltStylesheet));
    }

    public String transform(InputStream input) throws SaxonApiException {
        StringWriter outputString = new StringWriter();
        Serializer output = processor.newSerializer(outputString);

        Xslt30Transformer transformer = this.stylesheet.load30();
        transformer.transform(new StreamSource(input), output);

        return outputString.toString();
    }

    final private Processor processor = new Processor(false);
    final private XsltCompiler xsltCompiler;
    final private XsltExecutable stylesheet;
}
