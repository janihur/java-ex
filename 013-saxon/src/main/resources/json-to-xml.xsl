<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml input-01.json.xml?>
<xsl:stylesheet version="3.0"
	xmlns:a="http://contact.example.com"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:math="http://www.w3.org/2005/xpath-functions/math" xmlns:array="http://www.w3.org/2005/xpath-functions/array" xmlns:map="http://www.w3.org/2005/xpath-functions/map" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:err="http://www.w3.org/2005/xqt-errors" exclude-result-prefixes="array fn map math xhtml xs err"
>
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

	<xsl:template match="/json" name="xsl:initial-template">
		<!--<xsl:sequence select="fn:json-to-xml(.)"/>-->
		<xsl:apply-templates select="fn:json-to-xml(.)"/>
	</xsl:template>
	
	<xsl:template match="fn:map">
		<a:contacts>
			<xsl:apply-templates select="fn:array[@key = 'contacts']/fn:map" mode="contact"/>
		</a:contacts>
	</xsl:template>
	
	<xsl:template match="fn:map" mode="contact">
		<a:contact>
			<a:firstName>
				<xsl:value-of select="fn:string[@key = 'firstName']"/>
			</a:firstName>
			<a:lastName>
				<xsl:value-of select="fn:string[@key = 'lastName']"/>
			</a:lastName>
		</a:contact>	
	</xsl:template>
</xsl:stylesheet>
