<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml input-01.xml?>
<!-- convert XML to JSON -->
<xsl:stylesheet version="3.0"
	xmlns:a="http://contact.example.com"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:math="http://www.w3.org/2005/xpath-functions/math"
	xmlns:array="http://www.w3.org/2005/xpath-functions/array"
	xmlns:map="http://www.w3.org/2005/xpath-functions/map"
	exclude-result-prefixes="#all"
>
	<!-- XML representation of JSON -->
	<!--<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>-->
	<!-- JSON -->
	<xsl:output method="text"/>
	
	<xsl:template match="a:contacts" name="xsl:initial-template">
		<xsl:variable name="xml-to-json-input">
			<fn:map>
				<fn:array key="contacts">
					<xsl:apply-templates select="a:contact"/>
				</fn:array>
			</fn:map>
		</xsl:variable>

		<!-- XML representation of JSON -->
		<!--<xsl:sequence select="$xml-to-json-input"/>-->
		<!-- JSON -->
		<xsl:value-of select="fn:xml-to-json($xml-to-json-input, map{'indent': fn:true()})"/>
	</xsl:template>
	
	<xsl:template match="a:contact">
		<fn:map>
			<fn:string key="name">
				<xsl:value-of select="fn:string-join((a:firstName, a:lastName), ' ')"/>
			</fn:string>
			<xsl:sequence select="a:optional('company', a:companyName)"/>
			<xsl:sequence select="a:optional('phone', a:phoneNumber)"/>
			<xsl:sequence select="a:optional('email', a:email)"/>
			<xsl:apply-templates select="a:address"/>
		</fn:map>
	</xsl:template>
	
	<xsl:template match="a:address">
		<fn:map key="address">
			<xsl:sequence select="a:optional('street', fn:string-join(a:street, ' '))"/>
			<fn:string key="city">
				<xsl:value-of select="a:city"/>
			</fn:string>
			<fn:string key="zip">
				<xsl:value-of select="a:postalCode"/>
			</fn:string>
			<fn:string key="country">
				<xsl:value-of select="a:country"/>
			</fn:string>
		</fn:map>
	</xsl:template>
	
	<xsl:function name="a:optional">
		<xsl:param name="toName" as="xs:string"/>
		<xsl:param name="value" as="xs:string?"/>
		<xsl:if test="$value != ''">
			<xsl:element name="fn:string">
				<xsl:attribute name="key" select="$toName"/>
				<xsl:value-of select="$value"/>
			</xsl:element>
		</xsl:if>
	</xsl:function>

</xsl:stylesheet>
