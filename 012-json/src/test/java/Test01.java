import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.camel.util.json.DeserializationException;
import org.apache.camel.util.json.JsonArray;
import org.apache.camel.util.json.JsonObject;
import org.apache.camel.util.json.Jsoner;
import org.junit.Test;

import java.util.Set;

public class Test01 {
    private final String INPUT_JSON = "{\"data\":{\"type\":\"TYPE\",\"id\":\"ID\",\"price\":\"5.50€\",\"attributes\":{\"key\":\"VALUE\"}}}";
    @Test
    public void createJsonWithCamelUtilJsonObject() {
        JsonObject attributesObject = new JsonObject();
        attributesObject.put("key", "VALUE");

        JsonObject dataObject = new JsonObject();
        dataObject.put("type", "TYPE");
        dataObject.put("id", "ID");
        dataObject.put("price", "5.50€");
        dataObject.put("attributes", attributesObject);

        JsonObject rootObject = new JsonObject();
        rootObject.put("data", dataObject);

        System.out.println("-----");
        System.out.println("org.apache.camel.util.json.JsonObject:");
        System.out.println(rootObject.toJson());
    }

    @Test
    public void createJsonWithCamelUtilJsonObject_2() {
        Set<String> idSet = Set.of("ID1", "ID2", "ID3");

        JsonArray idList = new JsonArray();
        idList.addAll(idSet);

        JsonObject rootObject = new JsonObject();
        rootObject.put("idList", idList);

        System.out.println("-----");
        System.out.println("org.apache.camel.util.json.JsonObject:");
        System.out.println(rootObject.toJson());
    }

    @Test
    public void readJsonWithCamelUtilJsoner() {
        try {
            JsonObject rootNode = (JsonObject) Jsoner.deserialize(INPUT_JSON);
            JsonObject dataNode = (JsonObject) rootNode.get("data");
            String type = dataNode.get("type").toString();
            String price = dataNode.get("price").toString();
            System.out.println("-----");
            System.out.println("Read Json With org.apache.camel.util.json.Jsoner");
            System.out.println("type: " + type);
            System.out.println("price: " + price);
        } catch (DeserializationException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void createJsonWithJacksonObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();

        ObjectNode attributesObject = mapper.createObjectNode();
        attributesObject.put("key", "VALUE");

        ObjectNode rootObject = mapper.createObjectNode();
        rootObject.put("type", "TYPE");
        rootObject.put("id", "ID");
        rootObject.put("price", "5.50€"); // € is escaped
        rootObject.set("attributes", attributesObject);

        try {
            System.out.println("-----");
            System.out.println("com.fasterxml.jackson.databind.ObjectMapper:");
            System.out.println(mapper.writerWithDefaultPrettyPrinter().withRootName("data").writeValueAsString(rootObject));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void readJsonWithJackson() {
        ObjectMapper mapper = new ObjectMapper();

        try {
            JsonNode rootNode = mapper.readTree(INPUT_JSON);
            JsonNode dataNode = rootNode.get("data");
            String type = dataNode.get("type").asText();
            String price = dataNode.get("price").asText();
            System.out.println("-----");
            System.out.println("Read Json With Jackson:");
            System.out.println("type: " + type);
            System.out.println("price: " + price);
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
