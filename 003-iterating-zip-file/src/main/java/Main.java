import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public final class Main {
    final private static Predicate<String> isSystemProject =
	name -> (name.toLowerCase().contains("default/") ||
		 name.toLowerCase().contains("infrautility/") ||
		 name.toLowerCase().contains("system/"));

    public static List<ReadValue> readZipFile(byte[] zipFileBytes, ReaderInterface[] readers) {
	List<ReadValue> readValues = new ArrayList<ReadValue>();

	try {
	    ZipInputStream zipFile =
		new ZipInputStream(new ByteArrayInputStream(zipFileBytes));
	    ZipEntry ze = zipFile.getNextEntry();

	    int zIndex = 0;
	    while (ze != null) {
		zIndex++;
		String name = ze.getName();

		// process only files of interest of non-system projects
		if (isSystemProject.negate().test(name)) {
		    for (ReaderInterface reader : readers) {
			if (reader.scope().test(name)) {
			    System.err.printf("READING (%d): %s%n", zIndex, name);

			    ByteArrayOutputStream zipEntryBytes = new ByteArrayOutputStream();
			    byte[] buffer = new byte[1024];
			    int len = 0;
			    while ((len = zipFile.read(buffer)) > 0) {
				zipEntryBytes.write(buffer, 0, len);
			    }

			    readValues.add(reader.read(name, zipEntryBytes));
			}
		    }
		}

		zipFile.closeEntry();
		ze = zipFile.getNextEntry();
	    }
	    zipFile.close();
	} catch (Exception ex) {
	    System.err.println("Exception:");
	    System.err.println(ex);
	}

	return readValues;
    }

    public static void main(String[] args) {
	String environment = args[0];
	String zipFilePath = args[1];

	//
	// read zip file
	//

	byte[] zipFileBytes = null;
	try {
	    zipFileBytes = Files.readAllBytes(new File(zipFilePath).toPath());
	} catch (Exception ex) {
	    System.err.println("Exception:");
	    System.err.println(ex);
	}

	System.err.println("File size: " + zipFileBytes.length + " bytes");

	//
	// read the interesting data from the zip
	//

	ReaderInterface[] readers = {new ConfigReader(environment), new MetadataReader()};

	List<ReadValue> readValues = readZipFile(zipFileBytes, readers);

	//
	// process the return value
	//

	// sorted so that CONFIG is before CONFIG_ENV and the potential
	// attribute overrides go in right direction
	Collections.sort(readValues);

	Map<String, Map<String, String>> metadata = new HashMap<>();
	Map<String, Map<String, String>> config = new HashMap<>();

	for (ReadValue v : readValues) {
	    if (v.type.equals("CONFIG") || v.type.equals("CONFIG_ENV")) {
		if (config.containsKey(v.projectName)) {
		    config.get(v.projectName).putAll(v.attributes);
		} else {
		    config.put(v.projectName, v.attributes);
		}
	    } else if (v.type.equals("METADATA")) {
		if (metadata.containsKey(v.projectName)) {
		    metadata.get(v.projectName).putAll(v.attributes);
		} else {
		    metadata.put(v.projectName, v.attributes);
		}
	    }
	}

	System.err.println(metadata);
	System.err.println(config);
    }
}
