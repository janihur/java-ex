import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.function.Predicate;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

class ConfigReader implements ReaderInterface {
    @Override
    public Predicate<String> scope() {
	return new Predicate<String>() {
	    @Override
	    public boolean test(String str) {
		return
		    str.contains("/config/config.XML") ||
		    str.contains("/config/config_" + environment + ".XML");
	    }
	};
    };

    @Override
    public ReadValue read(String name, ByteArrayOutputStream bytes) {
	String projectName = name.split("/")[0];
	String configType = name.contains("/config/config.XML") ? "CONFIG" : "CONFIG_ENV";
	
	ReadValue rv = new ReadValue(configType, projectName);

	try {
	    Document wrapperDoc =
		builder.parse(new ByteArrayInputStream(bytes.toByteArray()));

	    NodeList nodes =
		wrapperDoc.getElementsByTagNameNS("http://www.bea.com/wli/sb/resources/config",
						  "xml-content");
	    Node xmlContent = nodes.item(0);
	    Node configNode = xmlContent.getFirstChild();
	    String configStr = configNode.getTextContent();

	    Document configDoc = builder.parse(new ByteArrayInputStream(configStr.getBytes("UTF-8")));
	    NodeList configNodes = configDoc.getDocumentElement().getChildNodes();

	    for (int i = 0; i < configNodes.getLength(); i++) {
		Node n = configNodes.item(i);
		if (n.getNodeType() == Node.ELEMENT_NODE) {
		    NamedNodeMap attrs = n.getAttributes();
		    rv.attributes.put(attrs.getNamedItem("key").getNodeValue(), n.getTextContent());
		}
	    }
	} catch (Exception ex) {
	    System.err.println("Exception:");
	    System.err.println(ex);
	}

	return rv;
    };

    public ConfigReader(String environment) {
	try {
	    factory = DocumentBuilderFactory.newInstance();
	    factory.setNamespaceAware(true);
	    builder = factory.newDocumentBuilder();
	} catch (Exception ex) {
	    System.err.println("Exception:");
	    System.err.println(ex);
	}

	this.environment = environment.toUpperCase();
    }

    private DocumentBuilderFactory factory;
    private DocumentBuilder builder;
    final private String environment;
}
