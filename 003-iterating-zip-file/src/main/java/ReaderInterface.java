import java.io.ByteArrayOutputStream;
import java.util.function.Predicate;

public interface ReaderInterface {
    public Predicate<String> scope();
    public ReadValue read(String name, ByteArrayOutputStream bytes);
}
