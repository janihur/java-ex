import java.util.HashMap;
import java.util.Map;

class ReadValue implements Comparable<ReadValue> {
    final public String type;
    final public String projectName;
    final public Map<String, String> attributes;

    public ReadValue(String type, String projectName) {
	this.type = type;
	this.projectName = projectName;
	this.attributes = new HashMap<String, String>();
    }

    @Override
    public int compareTo(ReadValue rv) {
	int cmp = this.projectName.compareTo(rv.projectName);
	if (0 == cmp) {
	    cmp = this.type.compareTo(rv.type);
	}

	return cmp;
    }

    @Override
    public String toString() {
	return
	    "(type = " + this.type +
	    ")(projectName = " + this.projectName +
	    ")(attributes = " + this.attributes +
	    ")";
    }
}
