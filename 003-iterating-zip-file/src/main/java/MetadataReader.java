import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

class MetadataReader implements ReaderInterface {
    @Override
    public Predicate<String> scope() {
	return new Predicate<String>() {
	    @Override
	    public boolean test(String str) {
		return
		    str.contains("/config/meta.XML") ||
		    str.contains("/_projectdata.LocationData");
	    }
	};
    };

    @Override
    public ReadValue read(String name, ByteArrayOutputStream bytes) {
	String projectName = name.split("/")[0];
	String configType = "METADATA";

	ReadValue rv = new ReadValue(configType, projectName);

	try {
	    Document wrapperDoc =
		builder.parse(new ByteArrayInputStream(bytes.toByteArray()));

	    if (name.contains("/config/meta.XML")) {
		NodeList nodes =
		    wrapperDoc.getElementsByTagNameNS("http://www.bea.com/wli/sb/resources/config",
						      "xml-content");
		Node xmlContent = nodes.item(0);
		Node metaNode = xmlContent.getFirstChild();
		String metaStr = metaNode.getTextContent();

		Document metaDoc = builder.parse(new ByteArrayInputStream(metaStr.getBytes("UTF-8")));
		NodeList metaNodes = metaDoc.getDocumentElement().getChildNodes();

		for (int i = 0; i < metaNodes.getLength(); i++) {
		    Node n = metaNodes.item(i);
		    if (n.getNodeType() == Node.ELEMENT_NODE) {
			rv.attributes.put(n.getLocalName(), n.getTextContent());
		    }
		}
	    } else if (name.contains("/_projectdata.LocationData")) {
		NodeList nodes =
		    wrapperDoc.getElementsByTagNameNS("http://www.bea.com/wli/config/project",
						      "description");
		if (nodes.getLength() > 0) {
		    Node xmlDescription = nodes.item(0);
		    String description = xmlDescription.getTextContent();
		    Matcher matcher = versionPattern.matcher(description);
		    String version = matcher.find() ? matcher.group(1) : null;

		    if (version != null) {
			rv.attributes.put("projectdata-version", version);
		    }
		}
	    }
	} catch (Exception ex) {
	    System.err.println("Exception:");
	    System.err.println(ex);
	}

	return rv;
    };

    public MetadataReader() {
	try {
	    factory = DocumentBuilderFactory.newInstance();
	    factory.setNamespaceAware(true);
	    builder = factory.newDocumentBuilder();
	} catch (Exception ex) {
	    System.err.println("Exception:");
	    System.err.println(ex);
	}

	versionPattern = Pattern.compile("^([\\d.]+)");
    }

    private DocumentBuilderFactory factory;
    private DocumentBuilder builder;
    final private Pattern versionPattern;
}
