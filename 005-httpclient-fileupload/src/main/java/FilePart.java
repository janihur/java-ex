import java.io.InputStream;

public class FilePart {
    final public String partName;
    final public String fileName;
    final public InputStream fileContent;

    public FilePart(String partName,
		    String fileName,
		    InputStream fileContent) {
	this.partName = partName;
	this.fileName = fileName;
	this.fileContent = fileContent;
    }
}
