import java.nio.charset.StandardCharsets;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;

public class Main {
    public static HttpEntity multipartBody(FilePart[] fileparts,
					   TextPart[] textparts)
	throws java.io.IOException
    {
	MultipartEntityBuilder multipart = MultipartEntityBuilder.create()
	    .setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
	    .setCharset(StandardCharsets.UTF_8);

	for (FilePart part : fileparts) {
	    multipart.addBinaryBody(part.partName,
				    part.fileContent,
				    ContentType.DEFAULT_BINARY,
				    part.fileName);
	}

	for (TextPart part : textparts) {
	    multipart.addTextBody(part.name,
				  part.text,
				  ContentType.DEFAULT_BINARY);
	}

	HttpEntity data = multipart.build();

	return data;
    }
}
