public class TextPart {
    final public String name;
    final public String text;

    public TextPart(String name,
		    String text) {
	this.name = name;
	this.text = text;
    }
}
