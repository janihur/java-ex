import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import org.junit.Test;

public class Test01 {
    private InputStream readResourceFile(String filename) {
	ClassLoader classLoader = getClass().getClassLoader();
	InputStream in = classLoader.getResourceAsStream(filename);
	return in;
    }

    // @Test
    public void test01() {
	try {
	    InputStream fileStream = readResourceFile("kitten-1.jpg");
	    assertNotNull(fileStream);

	    FilePart[] fileparts =
		{ new FilePart("file", "söpö kisuli !.jpg", fileStream) };

	    TextPart[] textparts =
		{ new TextPart("Part 1", "Text for Part One"),
		  new TextPart("Part 2", "Text for Part Two"),
		  new TextPart("Part 3", "Text for Part Three") };

	    HttpEntity e = Main.multipartBody(fileparts, textparts);

	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    e.writeTo(baos);

	    System.out.println(baos.toString(StandardCharsets.UTF_8.name()));
	} catch (Exception ex) {
	    fail();
	}
    }

    // @Test
    public void test02() {
	try {
	    HttpClient client = HttpClientBuilder.create().build();
	    HttpResponse response =
		client.execute(new HttpGet("http://httpbin.org/get"));
	    int statusCode = response.getStatusLine().getStatusCode();
	    assertThat(statusCode, equalTo(HttpStatus.SC_OK));
	} catch (Exception ex) {
	    fail();
	}
    }

    @Test
    public void test03() {
	try {
	    InputStream fileStream = readResourceFile("kitten-1.jpg");
	    assertNotNull(fileStream);

	    FilePart[] fileparts =
		{ new FilePart("file", "söpö kisuli !.jpg", fileStream) };

	    TextPart[] textparts =
		{ new TextPart("Part 1", "Text for Part One"),
		  new TextPart("Part 2", "Text for Part Two"),
		  new TextPart("Part 3", "Text for Part Three") };

	    HttpEntity e = Main.multipartBody(fileparts, textparts);

	    HttpClient client = HttpClientBuilder.create().build();

	    HttpPost post = new HttpPost("http://httpbin.org/post");
	    post.setEntity(e);

	    System.out.println(post.toString());

	    HttpResponse response = client.execute(post);

	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    response.getEntity().writeTo(baos);
	    System.out.println(baos.toString(StandardCharsets.UTF_8.name()));
			       
	    int statusCode = response.getStatusLine().getStatusCode();
	    assertThat(statusCode, equalTo(HttpStatus.SC_OK));
	} catch (Exception ex) {
	    fail();
	}
    }
}
