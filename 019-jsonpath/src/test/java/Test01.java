import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class Test01 {
    @Test
    @DisplayName("Read list of values to java.util.List")
    public void test_01() throws Exception {
        String inputStr = Files.readString(Path.of("src/test/resources", "input-01.json"));
        DocumentContext input = JsonPath.parse(inputStr);
        @SuppressWarnings("unchecked")
        List<String> idList = input.read("$.root.contacts[*].id", List.class);
        // System.err.println(idList);

        List<String> expected = List.of("ID1", "ID2", "ID3", "ID4");
        assertIterableEquals(expected, idList);
    }

    @Test
    @DisplayName("Read list of objects")
    public void test_02() throws Exception {
        String inputStr = Files.readString(Path.of("src/test/resources", "input-02.json"));
        DocumentContext input = JsonPath.parse(inputStr);
        @SuppressWarnings("unchecked")
        List<Map<String, Object>> contacts = input.read("$.root.contacts[*]", List.class);
        // System.err.println(contacts);

        assertEquals("ID3", contacts.get(2).get("id"));
        assertEquals("Charlie", contacts.get(2).get("name"));
        assertEquals(Map.of("id", "ID3", "name", "Charlie"), contacts.get(2));
    }

    @Test
    @DisplayName("java.util.Set operations")
    public void test_99() {
        {
            List<String> list1 = List.of("ID1", "ID2", "ID3", "ID4");
            List<String> list2 = List.of("ID1", "ID2"       , "ID4");

            // how to convert a List to a Set
            Set<String> set1 = new HashSet<>(list1);
            Set<String> set2 = new HashSet<>(list2);

            // set1 will contain intersection of set1 and set2
            assertEquals(true, set1.retainAll(set2)); // set1 is modified
            // System.err.println(set1);
            assertEquals(Set.of("ID1", "ID2", "ID4"), set1);
        }
        {
            Set<String> set1 = new HashSet<>(List.of("ID1", "ID2")); // a mutable set
            Set<String> set2 = Set.of("ID2", "ID3", "ID4"); // Set.of creates an immutable set

            // set1 will contain union of set1 and set2
            assertEquals(true, set1.addAll(set2)); // set1 is modified
            // System.err.println(set1);
            assertEquals(Set.of("ID1", "ID2", "ID3", "ID4"), set1);
        }
        {
            Set<String> set1 = new HashSet<>(List.of("ID1", "ID2", "ID3")); // a mutable set
            Set<String> set2 = Set.of("ID2", "ID3", "ID4"); // Set.of creates an immutable set

            // set1 will contain difference: set1 - set2
            assertEquals(true, set1.removeAll(set2)); // set1 is modified
            // System.err.println(set1);
            assertEquals(Set.of("ID1"), set1);
        }
        {
            Set<String> set1 = new HashSet<>(List.of("ID1", "ID2", "ID3")); // a mutable set

            // set1 will contain difference: set1 - "ID2"
            assertEquals(true, set1.remove("ID2")); // set1 is modified
            // System.err.println(set1);
            assertEquals(Set.of("ID1", "ID3"), set1);
        }
    }
}
