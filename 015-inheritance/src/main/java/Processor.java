public interface Processor {
    void process(Data data) throws Exception;
}
