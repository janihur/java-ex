public class Main {
    /*
    FooProcessor class (that configures the processing for "Foo")
      extends
        AbstractProcessor class (that provides the (common) implementation)
          implements
            Processor interface (that provides the contract)
     */
    public static void main(String[] args) {
        Data d = new Data();
        try {
            FooProcessor f = new FooProcessor(); // default step
            f.process(d);
            System.out.println(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            FooProcessor f = new FooProcessor(84);
            f.process(d);
            System.out.println(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            process(new FooProcessor(168), d);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static <T extends Processor> void process(T processor, Data d) throws Exception {
        processor.process(d);
        System.out.println(d);
    }
}
