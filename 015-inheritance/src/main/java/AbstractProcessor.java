public abstract class AbstractProcessor implements Processor {
    protected int step;
    protected String notePrefix;
    @Override
    public void process(Data data) throws Exception {
        data.counter += this.step;
        data.notes.add(this.notePrefix + ": counter increased");
    }
}
