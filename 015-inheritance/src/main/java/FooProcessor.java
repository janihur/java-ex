public class FooProcessor extends AbstractProcessor {
    public FooProcessor() {
        this.step = 42;
        this.notePrefix = "foo step 42";
    }

    public FooProcessor(int step) {
        this.step = step;
        this.notePrefix = "foo step manually set to " + String.valueOf(step);
    }
}
