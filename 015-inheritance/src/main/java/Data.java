import java.util.ArrayList;
import java.util.List;

public class Data {
    public int counter = 0;
    public List<String> notes = new ArrayList<>();

    @Override
    public String toString() {
        return "Data{" +
            "counter=" + counter +
            ", notes=" + notes +
            '}';
    }
}
